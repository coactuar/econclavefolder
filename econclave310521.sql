-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 08:48 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `econclave310521`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/05/28 15:04:55', '2021-05-28', '2021-05-28', 1, 'Abbott'),
(2, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/29 18:59:01', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(3, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/29 19:08:43', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(4, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021/05/29 19:54:14', '2021-05-29', '2021-05-29', 1, 'Abbott'),
(5, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/05/31 17:26:14', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(6, 'Jebin John', 'jebin_j.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/31 19:02:13', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(7, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/31 19:08:02', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(8, 'Dipankar Das', 'dipankar.das@abbott.com', 'Kolkata', 'Abbott Vascular', NULL, NULL, '2021/05/31 19:22:04', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(9, 'Atanu Banerjee', 'atanu.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/05/31 19:26:44', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(10, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021/05/31 19:28:22', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(11, 'HAZRA hazra', 'phpkhazra@gmail.com', 'KOLKATA', 'Amri', NULL, NULL, '2021/05/31 19:33:10', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(12, 'Shouvik Bhattacharya', 'shouvik.bhattacharya@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/05/31 19:36:03', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(13, 'Rahul kolhekar ', 'rahul.vkolhekar@abbott.com', 'Kolkata ', 'AV', NULL, NULL, '2021/05/31 19:37:46', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(14, 'Vibhutendra Mohanty', 'drvm7578@gmail.com', 'Bhubaneswar', 'Sum Ultimate Medicare', NULL, NULL, '2021/05/31 19:47:13', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(15, 'Dr. Shamshad Alam', 'shamshadkasper@gmail.com', 'patna', 'Ruban Memorial Hospital', NULL, NULL, '2021/05/31 19:50:33', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(16, 'Vibhutendra Mohanty', 'drvm7578@gmail.com', 'Bhubaneswar', 'Sum Ultimate Medicare', NULL, NULL, '2021/05/31 19:50:40', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(17, 'Vibhutendra Mohanty', 'drvm7578@gmail.com', 'Bhubaneswar', 'Sum Ultimate Medicare', NULL, NULL, '2021/05/31 19:53:14', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(18, 'shuvanan ray', 'shuvananray@gmail.com', 'kolkata', 'fortis', NULL, NULL, '2021/05/31 19:53:25', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(19, 'Sachit Roy', 'sachit.roy@abbott.com', 'Patna', 'Abbott Vascular', NULL, NULL, '2021/05/31 19:55:24', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(20, 'Ayan Kar', 'dr.ayan.kar@gmail.com', 'Kolkata', 'NH-RTIICS', NULL, NULL, '2021/05/31 20:01:46', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(21, 'Sumanta  Padhi ', 'sumantapadhi@yahoo.com', 'RAIPUR', 'NH MMI', NULL, NULL, '2021/05/31 20:02:17', '2021-05-31', '2021-05-31', 1, 'Abbott'),
(22, 'Anupam Jena', 'dranupamjena@gmail.com', 'BHUBANESWAR', 'KALINGA INSTITUTE OF MEDICAL SCIENCES', NULL, NULL, '2021/05/31 20:37:23', '2021-05-31', '2021-05-31', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Dibyajyoti Mishra', 'dibyajyoti.mishra@kims.ac.in', 'Sir PDA post dilated with 3.75 ????', '2021-05-31 21:27:35', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-28 15:04:28', '2021-05-28 15:04:28', '2021-05-28 15:04:38', 0, 'Abbott', '4a0f72fddaef54362dce053769f061d9'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-28 16:25:56', '2021-05-28 16:25:56', '2021-05-28 16:26:17', 0, 'Abbott', 'c270d22225ddb4cb02d2738924e71185'),
(3, 'Ayan Kar', 'dr.ayan.kar@gmail.com', 'Kolkata', 'NH-RTIICS', NULL, NULL, '2021-05-30 20:21:05', '2021-05-30 20:21:05', '2021-05-30 21:51:05', 0, 'Abbott', '39b1358bc85b3195147f3716a24c75ea'),
(4, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-31 17:25:35', '2021-05-31 17:25:35', '2021-05-31 17:25:43', 0, 'Abbott', 'bf107536fb873ee8dda35953962d6343'),
(5, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-31 19:07:23', '2021-05-31 19:07:23', '2021-05-31 20:37:23', 1, 'Abbott', 'e2968e30b6bfa2ad3ce594c54d1e8c98'),
(6, 'Vibhutendra Mohanty', 'drvm7578@gmail.com', 'Bhubaneswar', 'Sum Ultimate Medicare', NULL, NULL, '2021-05-31 19:43:34', '2021-05-31 19:43:34', '2021-05-31 21:13:34', 1, 'Abbott', '95e23edc8174bd22b9012ecce816e0f4'),
(7, 'Subhabrata Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-05-31 19:55:12', '2021-05-31 19:55:12', '2021-05-31 21:25:12', 1, 'Abbott', 'bf9ef8b4f2a88d9c8a1315d1aa0178c7'),
(8, 'MANAS RANJAN BARIK', 'manasbarik86@gmail.com', 'BHUBANESWAR', 'SUM ULTIMATE MEDICARE', NULL, NULL, '2021-05-31 19:57:35', '2021-05-31 19:57:35', '2021-05-31 21:27:35', 1, 'Abbott', '107d12e97e1b953fc5990a04305c9a6c'),
(9, 'Shouvik Bhattacharya', 'shouvik.bhattacharya@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-05-31 19:57:41', '2021-05-31 19:57:41', '2021-05-31 21:27:41', 1, 'Abbott', '8cad4d54dba9fdfb83a53e8f0962a1c1'),
(10, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-31 19:59:09', '2021-05-31 19:59:09', '2021-05-31 21:54:34', 0, 'Abbott', '6b3aba66e51c6508fd592b7d0b6499a0'),
(11, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bbsr', 'AV', NULL, NULL, '2021-05-31 19:59:51', '2021-05-31 19:59:51', '2021-05-31 21:29:51', 1, 'Abbott', '7e2a09ad7bc05eeb0fb87d6b3f7480ce'),
(12, 'Manas Ranjan Barik', 'manasbarik86@gmail.com', 'Bhubaneswar', 'SUM ULTIMATE', NULL, NULL, '2021-05-31 20:01:50', '2021-05-31 20:01:50', '2021-05-31 20:06:50', 0, 'Abbott', 'f4f80c5e0b93c77396127f42f905424d'),
(13, 'Satyaranjan  Mohanty', 'satya.bapun@gmail.com', 'BHUBANESWAR', 'Apollo Hospitals ', NULL, NULL, '2021-05-31 20:04:18', '2021-05-31 20:04:18', '2021-05-31 21:34:18', 1, 'Abbott', 'ee69f6c24c13c19c24c8031b4fa25743'),
(14, 'dr sarat kumae sahoo', 'drsaratsahoo@rediffmail.com', 'bhubaneswar', 'sumum', NULL, NULL, '2021-05-31 20:04:23', '2021-05-31 20:04:23', '2021-05-31 21:34:23', 1, 'Abbott', '6034593dcb9134917270679f688e992f'),
(15, 'Atanu Banerjee ', 'atanu.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-05-31 20:04:26', '2021-05-31 20:04:26', '2021-05-31 21:34:26', 1, 'Abbott', '5d8ae2f85214cd644c9131d62c525d7d'),
(16, 'MANAS RANJAN BARIK', 'manasbarik86@gmail.com', 'BHUBANESWAR', 'SUM ULTIMATE MEDICARE', NULL, NULL, '2021-05-31 20:06:18', '2021-05-31 20:06:18', '2021-05-31 21:36:18', 1, 'Abbott', 'b42f68f2804143d9659b5d822a3aca4f'),
(17, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangalore', NULL, NULL, '2021-05-31 20:07:09', '2021-05-31 20:07:09', '2021-05-31 20:07:25', 0, 'Abbott', 'e2eb2ef28bf7842eab8752e01e2bdd16'),
(18, 'Rahul Kolhekar ', 'rahul.vkolhekar@abbott.com', 'Kolkata ', 'AV', NULL, NULL, '2021-05-31 20:07:28', '2021-05-31 20:07:28', '2021-05-31 20:30:19', 0, 'Abbott', 'c83a30c93443f1f1e13a4ebf630454b1'),
(19, 'Soumen Nandi	', 'saumennandi@gmail.com', 'Kolkata', 'NRS Medical College	', NULL, NULL, '2021-05-31 20:08:21', '2021-05-31 20:08:21', '2021-05-31 21:38:21', 1, 'Abbott', '03621798d7ec4432bedfa976e3b05f00'),
(20, 'Dr Chandan Modak', 'chandan.dr.modak@yahoo.com', 'Guwahati', 'Dispur Hospital', NULL, NULL, '2021-05-31 20:08:55', '2021-05-31 20:08:55', '2021-05-31 21:38:55', 1, 'Abbott', '1e4036654442142e3402c30cc73e194d'),
(21, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV', NULL, NULL, '2021-05-31 20:09:31', '2021-05-31 20:09:31', '2021-05-31 21:39:31', 1, 'Abbott', 'bd834d6530335884963651b6ad1fdd23'),
(22, 'Koushik Dasgupta', 'KOUSHIK_NMC@YAHOO.COM', 'KOLKATA', 'nh rtiics', NULL, NULL, '2021-05-31 20:09:36', '2021-05-31 20:09:36', '2021-05-31 21:39:36', 1, 'Abbott', '88cfc5bf4db8097083417716e13d1940'),
(23, 'Dr K C Narzary', 'drkrishna_narzary@yahoo.co.in', 'Guwahati', 'GNRC', NULL, NULL, '2021-05-31 20:10:11', '2021-05-31 20:10:11', '2021-05-31 21:40:11', 1, 'Abbott', '6a1916bb6d20d2358539ccd561a44509'),
(24, 'Avijit Das', 'avijit.das1@abbott.com', 'KOLKATA', 'None', NULL, NULL, '2021-05-31 20:10:13', '2021-05-31 20:10:13', '2021-05-31 21:40:13', 1, 'Abbott', '7a759a12de8dadf7082ab71bad79bc7b'),
(25, 'Basabendra Chowdhury	', 'cbasabendra@gmail.com', 'kol', 'Calcutta Medical College	', NULL, NULL, '2021-05-31 20:10:22', '2021-05-31 20:10:22', '2021-05-31 21:40:22', 1, 'Abbott', '899b98b6739e7fd82b7b1af2bced324a'),
(26, 'Dr Shashank Baruah', 'sasankabaruah@yahoo.com', 'Guwahati', 'Healthcity Hospital', NULL, NULL, '2021-05-31 20:10:30', '2021-05-31 20:10:30', '2021-05-31 21:40:30', 1, 'Abbott', '121d7cf8ccec6d7aeb89f6bced8e88fe'),
(27, 'Dr Rajesh Das ', 'rajeshdasdr@gmail.com', 'Guwahati', 'NEMCARE ', NULL, NULL, '2021-05-31 20:10:46', '2021-05-31 20:10:46', '2021-05-31 21:40:46', 1, 'Abbott', 'b7f6b4d3ca8bd653ba1f1942acaad6f7'),
(28, 'Dr Ritankur Borkotoky', 'ritankur@gmail.com', 'Guwahati', 'NH ', NULL, NULL, '2021-05-31 20:11:09', '2021-05-31 20:11:09', '2021-05-31 21:41:09', 1, 'Abbott', '559e598de2ede03bba48383310b99bce'),
(29, 'Mukesh Kumar Agarwal	', 'mukeshrims@gmail.com', 'kolkata', 'Calcutta Medical College	', NULL, NULL, '2021-05-31 20:11:17', '2021-05-31 20:11:17', '2021-05-31 21:41:17', 1, 'Abbott', '89004533b014606bdc55ec8932a0b322'),
(30, 'Raju Rao', 'rajurao598@gmail.com', 'Guwahati', 'International Hospital', NULL, NULL, '2021-05-31 20:11:32', '2021-05-31 20:11:32', '2021-05-31 21:41:32', 1, 'Abbott', 'f1cd563d7d31e07ac71ebb9b36f0377e'),
(31, 'Souvik Roy	', 'drsouvikray913@gmail.com', 'KOL', 'NRS Medical College	', NULL, NULL, '2021-05-31 20:12:26', '2021-05-31 20:12:26', '2021-05-31 21:42:26', 1, 'Abbott', '19b5242c673d2807b1d7bf05765708e4'),
(32, 'Banjeet Kumar ', 'kbanjeet@gmail.com', 'Guwahati', 'International Hospital', NULL, NULL, '2021-05-31 20:13:35', '2021-05-31 20:13:35', '2021-05-31 21:43:35', 1, 'Abbott', '973360c3a43f8780141ef12a7a15728b'),
(33, 'Tirthakar Roy	', 'hopefultiro@gmail.com', 'KOL', 'RGKAR Medical College	', NULL, NULL, '2021-05-31 20:13:44', '2021-05-31 20:13:44', '2021-05-31 21:43:44', 1, 'Abbott', '3e5d7943c7fc01bbbcc5064759f9ea4a'),
(34, 'Tirthakar Roy	', 'hopefultiro@gmail.com', 'KOL', 'RGKAR Medical College	', NULL, NULL, '2021-05-31 20:13:44', '2021-05-31 20:13:44', '2021-05-31 21:43:44', 1, 'Abbott', '6668ba76017e494f79507c049ad674a5'),
(35, 'Dr Jayanta Saha ', 'drjayanta.saha2010@gmail.com', 'Kolkata', 'Calcutta Medical College ', NULL, NULL, '2021-05-31 20:14:02', '2021-05-31 20:14:02', '2021-05-31 21:44:02', 1, 'Abbott', '531b9ecfbbbd01c87be8a3f1a64fbbeb'),
(36, 'Himnaku Deka ', 'dhimanku@yahoo.com', 'Guwahati', 'GNRC', NULL, NULL, '2021-05-31 20:14:28', '2021-05-31 20:14:28', '2021-05-31 21:44:28', 1, 'Abbott', 'ab90e640b358247114c9f8e632c448ee'),
(37, 'Samir nayak', 'samirnayak703@gmail.com', 'Bhubaneswar', 'Apollo', NULL, NULL, '2021-05-31 20:14:41', '2021-05-31 20:14:41', '2021-05-31 21:44:41', 1, 'Abbott', 'c8b66d84cdd2561eeb352b6489bb4ce9'),
(38, 'Subhashish Chakraborty 	', 'subhashish.chak83@gmail.com', 'Kolkata', 'NRS Medical College	', NULL, NULL, '2021-05-31 20:14:55', '2021-05-31 20:14:55', '2021-05-31 21:44:55', 1, 'Abbott', 'b6e8da0d3dc0d6d0eef4ac3548a431d3'),
(39, 'Amitabha Ray', 'amitabha@saimed.in', 'Kolkata ', 'Saim', NULL, NULL, '2021-05-31 20:15:33', '2021-05-31 20:15:33', '2021-05-31 21:45:33', 1, 'Abbott', '4703c5ee4ae0f43e96dbdb2dbcddb936'),
(40, 'Enem Horo', 'enemhoro33@yahoo.com', 'Guwahati', 'GNRC', NULL, NULL, '2021-05-31 20:15:53', '2021-05-31 20:15:53', '2021-05-31 21:45:53', 1, 'Abbott', '3dcb4804a8383f4a7e1cfa18bbb82c56'),
(41, 'Akshay Dhore	', 'akshayddhore@gmail.com', 'Kolkata', 'RGKAR Medical College	', NULL, NULL, '2021-05-31 20:15:54', '2021-05-31 20:15:54', '2021-05-31 21:45:54', 1, 'Abbott', '6b1458f7a3f7cf2855dbeb592cd270ff'),
(42, 'Saurav Das', 'drsaurav1@gmail.com', 'Bhubhaneswar', 'KIMS ', NULL, NULL, '2021-05-31 20:17:10', '2021-05-31 20:17:10', '2021-05-31 21:47:10', 1, 'Abbott', 'e64b314c755a1ba6fe82236364dda03c'),
(43, 'S Alam	', 'sharwar2011@gmail.com', 'kolkata', 'SSKM Hospital', NULL, NULL, '2021-05-31 20:17:11', '2021-05-31 20:17:11', '2021-05-31 21:47:11', 1, 'Abbott', '5ffca6300d6d2cb5411d08d6135ebb8b'),
(44, 'Kunal Adak', 'kunaladak96@gmail.com', 'Bhubaneswar', 'Blue Wheel Hospital', NULL, NULL, '2021-05-31 20:17:52', '2021-05-31 20:17:52', '2021-05-31 21:47:52', 1, 'Abbott', '58a4fef041545dd3afd880f8c3490141'),
(45, 'Sandipan Sarkar 	', 'sandipan007.83@gmail.com', 'KOL', 'Bardhaman Medical College 	', NULL, NULL, '2021-05-31 20:18:08', '2021-05-31 20:18:08', '2021-05-31 21:48:08', 1, 'Abbott', 'b1602117019f67203a8ead8c57ec76c6'),
(46, 'Khilendra Bhagat', 'bhagatkhilendra@gmail.com', 'RAIPUR', 'Narayana Health', NULL, NULL, '2021-05-31 20:18:28', '2021-05-31 20:18:28', '2021-05-31 21:48:28', 1, 'Abbott', '62d01268c3184b13e54109ec23ca2d6b'),
(47, 'Bimal Kahar	', 'bimsleo@gmail.com', 'Kolkata', 'SSKM	', NULL, NULL, '2021-05-31 20:18:52', '2021-05-31 20:18:52', '2021-05-31 21:48:52', 1, 'Abbott', 'cf8be78ec95595166a89cb3794b95826'),
(48, 'partha paatim', 'Partha.partim@gmail.com', 'Jamshedpur ', 'Meditrina', NULL, NULL, '2021-05-31 20:19:55', '2021-05-31 20:19:55', '2021-05-31 21:49:55', 1, 'Abbott', 'd9ef06725e8855385aa228151f01e838'),
(49, 'Reja M Bora 	', 'rejawasif@gmail.com', 'Kolkata', 'Bardhaman Medical College 	', NULL, NULL, '2021-05-31 20:20:21', '2021-05-31 20:20:21', '2021-05-31 21:50:21', 1, 'Abbott', 'b5b8dfef82243676a123dcf070572cfd'),
(50, 'SOMNATH PATRA', 'SOMNATH.CATHTECH@GMAIL.COM', 'BHUBANESWAR', 'SUM ULTIMATE MEDICARE', NULL, NULL, '2021-05-31 20:21:57', '2021-05-31 20:21:57', '2021-05-31 21:51:57', 1, 'Abbott', 'fe85b60d6c8a24b6c0bd76444ec91bfe'),
(51, 'Adil Bashir 	', 'wasilab.85@gmail.com', 'Kolkata', 'NRS Medical College 	', NULL, NULL, '2021-05-31 20:23:00', '2021-05-31 20:23:00', '2021-05-31 21:53:00', 1, 'Abbott', 'a91b781023cd72ce2047ed42312a6129'),
(52, 'Dr Arijit Ghosh ', 'arijitjoy@gmail.com', 'Kolkata', 'AMRI Salt Lake ', NULL, NULL, '2021-05-31 20:23:19', '2021-05-31 20:23:19', '2021-05-31 21:53:19', 1, 'Abbott', '362deb2031188ef8fc5fdc373cfe8131'),
(53, 'Dr Aritra Konar ', 'konar1978.dr@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-05-31 20:23:43', '2021-05-31 20:23:43', '2021-05-31 21:53:43', 1, 'Abbott', 'e58487478ce3c717d9f6552aeceb7cbd'),
(54, 'Dr Asfaque Ahmed ', 'ahmed.asfaque@gmail.com', 'Kolkata', 'S S Chatterjee Hospital ', NULL, NULL, '2021-05-31 20:24:00', '2021-05-31 20:24:00', '2021-05-31 21:54:00', 1, 'Abbott', 'cdcedc8eedb259bce56fb9fa85512e8e'),
(55, 'Aninda Sarkar		', 'consult.cardio87@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-05-31 20:24:06', '2021-05-31 20:24:06', '2021-05-31 21:54:06', 1, 'Abbott', '8b511fb565d7cfc4d302644dc3b95a2e'),
(56, 'Dr Nabarun Roy', 'roy.nabarun@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-05-31 20:24:39', '2021-05-31 20:24:39', '2021-05-31 21:54:39', 1, 'Abbott', 'a376509a2e4f9759e6714b123805d301'),
(57, 'Dr Sachit Majumder ', 'Sachit.majumder@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-05-31 20:25:00', '2021-05-31 20:25:00', '2021-05-31 21:55:00', 1, 'Abbott', '61e22919d6e8ce762480cc9d7fa960e5'),
(58, 'Ankita Tibrewal		', 'ankittibdewal206@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-05-31 20:25:02', '2021-05-31 20:25:02', '2021-05-31 21:55:02', 1, 'Abbott', 'a047ead8d9a98532bb41f768d3d1c92d'),
(59, 'Dr Subhashis Roychowdhury ', 'Roychowdhury.s@gmail.com', 'Kolkata', 'AMRI Salt Lake ', NULL, NULL, '2021-05-31 20:25:28', '2021-05-31 20:25:28', '2021-05-31 21:55:28', 1, 'Abbott', '61357ef848ad84163718eb883ba70f1c'),
(60, 'Kaushik Manna 		', 'drkaushikmanna@gmail.com', 'Kolkata', 'SSKM', NULL, NULL, '2021-05-31 20:26:09', '2021-05-31 20:26:09', '2021-05-31 21:56:09', 0, 'Abbott', '5d5b2e15d3d4ba792db66c6444f77570'),
(61, 'Dr Debasis Ghosh ', 'ghosh.deba@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-05-31 20:26:11', '2021-05-31 20:26:11', '2021-05-31 21:56:11', 0, 'Abbott', '9b0733ae0c58241063d4b15b6a1daaac'),
(62, 'Dhananjay	', 'djvishalmya@gmail.com', 'kol', 'R G Kar Medical College 	', NULL, NULL, '2021-05-31 20:27:13', '2021-05-31 20:27:13', '2021-05-31 21:57:13', 0, 'Abbott', '682b3579f46279e5be8b01af20f6d2f0'),
(63, 'Sachit Roy', 'sachit.roy@abbott.com', 'Patna', 'AV', NULL, NULL, '2021-05-31 20:27:43', '2021-05-31 20:27:43', '2021-05-31 21:57:43', 0, 'Abbott', '2035a7c1bf4d95d19087968abc5c6b05'),
(64, 'Dr Sunip Banerjee ', 'sgcardiaccare@gmail.com', 'Kolkata', 'S G Cardiac Care ', NULL, NULL, '2021-05-31 20:27:56', '2021-05-31 20:27:56', '2021-05-31 21:57:56', 0, 'Abbott', 'c4a0ae1b6b48fc609f8cd71cf7aa00ea'),
(65, 'Dr Javed Ali khan', 'javedakhan@yahoo.com', 'Raipur', 'Care', NULL, NULL, '2021-05-31 20:30:45', '2021-05-31 20:30:45', '2021-05-31 20:45:02', 0, 'Abbott', 'c732086c6c596103e7fa19d283ef28c6'),
(66, 'Jahir Husan ', 'jahir.husen@gmail.com', 'Kolkata', 'nh', NULL, NULL, '2021-05-31 20:30:49', '2021-05-31 20:30:49', '2021-05-31 22:00:49', 0, 'Abbott', '455abdb363896602a058966d6085b4a9'),
(67, 'Mr Anupan Bhunai', 'anupam.bhunai@gmail.com', 'Jamshedpur', 'tat', NULL, NULL, '2021-05-31 20:31:22', '2021-05-31 20:31:22', '2021-05-31 22:01:22', 0, 'Abbott', '40ab8e6b4b74074935fa3a8792690750'),
(68, 'Vibhu', 'vibhu.kd09@gmail.com', 'Raipur', 'Ramkrishna hospital raipur', NULL, NULL, '2021-05-31 20:31:26', '2021-05-31 20:31:26', '2021-05-31 22:01:26', 0, 'Abbott', 'faa8c8337d20db2c109b0f42042c9847'),
(69, 'Snehil Goswami', 'snehil.1508@gmail.com', 'Raipur', 'NH MMI RAIPUR', NULL, NULL, '2021-05-31 20:31:29', '2021-05-31 20:31:29', '2021-05-31 22:01:29', 0, 'Abbott', 'a1b87b5596d05c108493bca7400eabd6'),
(70, 'Dr Abhay Krishna ', 'abhaykrishna@gmail.com', 'Jamshedpur', 'tata', NULL, NULL, '2021-05-31 20:31:37', '2021-05-31 20:31:37', '2021-05-31 22:01:37', 0, 'Abbott', '4d3a14e5c75a98985e9a62e84aba8d33'),
(71, 'Sunil Ch Roy', 'sunil.roy@abbott.com', 'BHUBANESWAR', 'AV', NULL, NULL, '2021-05-31 20:31:43', '2021-05-31 20:31:43', '2021-05-31 22:01:43', 0, 'Abbott', 'a11ad08fe2a4f08c3108d2aeab8663c0'),
(72, 'Dr P Singh', 'pankajsingh.67@hmail.com', 'Kolkata', 'RTIICS', NULL, NULL, '2021-05-31 20:31:50', '2021-05-31 20:31:50', '2021-05-31 22:01:50', 0, 'Abbott', '8d8fbf18fea72c3281be335f1ef72277'),
(73, 'Dr A K Parida ', 'parida.asoke@gmail.com', 'Durgapur ', 'HWH', NULL, NULL, '2021-05-31 20:31:59', '2021-05-31 20:31:59', '2021-05-31 22:01:59', 0, 'Abbott', '3834e9ede87ec5d37256102c4821608d'),
(74, 'Brojo', 'brojobilashthakur@gmail.com', 'kol', 'RTIICS', NULL, NULL, '2021-05-31 20:32:16', '2021-05-31 20:32:16', '2021-05-31 22:02:16', 0, 'Abbott', '16c6de875b9af14288dde3e6f107db3a'),
(75, 'Dr Joy Sanyal ', 'sanyal.joy@gmail.com', 'Durgapur ', 'Healthworld ', NULL, NULL, '2021-05-31 20:32:44', '2021-05-31 20:32:44', '2021-05-31 22:02:44', 0, 'Abbott', '56e72053fc8d0af6e5f34b8d5ed5ea17'),
(76, 'Sayan mondal', 'sayancathtech98@gmail.com', 'Bhubaneswar', 'AMRI', NULL, NULL, '2021-05-31 20:33:19', '2021-05-31 20:33:19', '2021-05-31 22:03:19', 0, 'Abbott', '063ace300163c01f6890db188c47ea23'),
(77, 'Dr Debargha Dhua', 'debargha.duah@gmail.com', 'Durgapur ', 'The Mission Hospital ', NULL, NULL, '2021-05-31 20:33:27', '2021-05-31 20:33:27', '2021-05-31 22:03:27', 0, 'Abbott', '5212db74d6d0ea0fafe3550c181ade69'),
(78, 'gour', 'gour.sharma4@gmail.com', 'Kolkata', 'amari mukundapur', NULL, NULL, '2021-05-31 20:33:39', '2021-05-31 20:33:39', '2021-05-31 22:03:39', 0, 'Abbott', '3ae611edd9213a21ecb80c1f3ddc8821'),
(79, 'Amitabha Ray', 'amitabha@saimed.in', 'Kolkata ', 'Sai', NULL, NULL, '2021-05-31 20:34:01', '2021-05-31 20:34:01', '2021-05-31 22:04:01', 0, 'Abbott', '6bb9d49cd432c8bf7996c13f9d445c1c'),
(80, 'Uttam Bera', 'uttam.bera@rediffmail.com', 'Durgapur ', 'The Mission Hospital ', NULL, NULL, '2021-05-31 20:34:07', '2021-05-31 20:34:07', '2021-05-31 22:04:07', 0, 'Abbott', '3ee34f2aecfb5f923316b8ffc36565ab'),
(81, 'Jayanta M', 'jayanta29@yahoo.com', 'Durgapur ', 'The Mission Hospital ', NULL, NULL, '2021-05-31 20:34:33', '2021-05-31 20:34:33', '2021-05-31 22:04:33', 0, 'Abbott', 'd2b48fff903edc3960f3b296e35d3003'),
(82, 'Satyajit Das', 'styajaitsamta@gmail.com', 'kolkata', 'nh', NULL, NULL, '2021-05-31 20:34:33', '2021-05-31 20:34:33', '2021-05-31 22:04:33', 0, 'Abbott', '9585ca8991e829b5f31b5ad156d3858a'),
(83, 'Justin Joshp', 'jastin.j30@gmail.com', 'Ranchi ', 'Pulse', NULL, NULL, '2021-05-31 20:35:20', '2021-05-31 20:35:20', '2021-05-31 22:05:20', 0, 'Abbott', 'ce93ff844d01323ac74a6fb3db03cc36'),
(84, 'Snehil Goswami', 'snehil.1508@gmail.com', 'Raipur', 'NH MMI RAIPUR', NULL, NULL, '2021-05-31 20:36:15', '2021-05-31 20:36:15', '2021-05-31 22:06:15', 0, 'Abbott', '02ebd4c124cd8de9f87447f8332c76f8'),
(85, 'T K Mishra', 'drtkmishra@yahoo.com', 'beh', 'MKCG', NULL, NULL, '2021-05-31 20:38:49', '2021-05-31 20:38:49', '2021-05-31 22:08:49', 0, 'Abbott', '005909ee25c0ad68b5132f593cccb09a'),
(86, 'Dr RK Mahanty', 'rkmahanty@gmail.com', 'Bbsr', 'Bwh', NULL, NULL, '2021-05-31 20:38:53', '2021-05-31 20:38:53', '2021-05-31 22:08:53', 0, 'Abbott', 'fd978b697c569621ed70b2144d991135'),
(87, 'Dr  ', 'drbijaydas@gmail.com', 'ctc', 'scb', NULL, NULL, '2021-05-31 20:41:05', '2021-05-31 20:41:05', '2021-05-31 22:11:05', 0, 'Abbott', '33c8430da0c94105d10d947b7885e354'),
(88, 'Santanu Bag', 'bag.santanu@yahoo.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-05-31 20:41:42', '2021-05-31 20:41:42', '2021-05-31 22:11:42', 0, 'Abbott', '4c328834915921ae7f31768cebdeb043'),
(89, 'Bala', 'bala.subramani@gmail.com', 'Kolkata', 'Apollo Hospital Kolkata', NULL, NULL, '2021-05-31 20:41:58', '2021-05-31 20:41:58', '2021-05-31 22:11:58', 0, 'Abbott', '6b0530882953662762927c182a693363'),
(90, 'Md Amir Akthar', 'amir.md@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-05-31 20:42:18', '2021-05-31 20:42:18', '2021-05-31 22:12:18', 0, 'Abbott', 'd87331a8f2ff08680560e756412d8a41'),
(91, 'Dr Ravi K', 'ravikachhela@gmail.com', 'Ranchi', 'Rampyari', NULL, NULL, '2021-05-31 20:42:45', '2021-05-31 20:42:45', '2021-05-31 22:12:45', 0, 'Abbott', '49c563961ad2b68249b17ab262fc27b1'),
(92, 'Sudip Saw ', 'sudip.shaw@gmail.com', 'Kolkata', 'Ruby ', NULL, NULL, '2021-05-31 20:42:46', '2021-05-31 20:42:46', '2021-05-31 22:12:46', 0, 'Abbott', '3082466e0f01e08a53b618f0cffee74e'),
(93, 'Ripon Sarkar', 'ripon.sarkarkol@gmail.com', 'Kolkata', 'S S Chatterjee Hospital ', NULL, NULL, '2021-05-31 20:43:38', '2021-05-31 20:43:38', '2021-05-31 22:13:38', 0, 'Abbott', '5390dd66f5b781b0dfcb2f91b7f96ac5'),
(94, 'Dr M P Samal', 'drsamal2000@yahoo.com', 'Bilaspur ', 'Apollo', NULL, NULL, '2021-05-31 20:45:30', '2021-05-31 20:45:30', '2021-05-31 22:15:30', 0, 'Abbott', '87a31619d3deb13a50d778b5a1ec16bc'),
(95, 'T K DAS', 'drtanmaykumar@gmail.com', 'Bhubaneswar', 'Care', NULL, NULL, '2021-05-31 20:46:06', '2021-05-31 20:46:06', '2021-05-31 22:16:06', 0, 'Abbott', 'da51d59645acff97f61c3b1f075eeaeb'),
(96, 'Lahu Gole', 'lahuthetechnician@yahoo.com', 'Raipur', 'Nhmmi ', NULL, NULL, '2021-05-31 20:47:20', '2021-05-31 20:47:20', '2021-05-31 22:17:20', 0, 'Abbott', '16d49f72d02ef25c912da1acd0edc3b2'),
(97, 'Dr Soumya rn Mohapatra', 'drsrmahapatra@gmail.com', 'Bhubaneswar', 'Sum', NULL, NULL, '2021-05-31 20:47:34', '2021-05-31 20:47:34', '2021-05-31 21:18:45', 0, 'Abbott', '7fedc0e85567a83a91569fccbe172067'),
(98, 'Kunal Adak', 'kunaladak96@gmail.com', 'Bhubaneswar', 'Blue Wheel Hospital', NULL, NULL, '2021-05-31 20:48:53', '2021-05-31 20:48:53', '2021-05-31 22:18:53', 0, 'Abbott', '8c970bf2071bde1c2740a4234bd8df4b'),
(99, 'Dr Alok', 'alokkumarssy@gmail.com', 'Patna', 'Jeevak', NULL, NULL, '2021-05-31 20:49:20', '2021-05-31 20:49:20', '2021-05-31 22:19:20', 0, 'Abbott', '919b7e6765dcd15ee8cbb9689c97e5fe'),
(100, 'Pramendra Kumar', 'krpramendra1982@gmail.com', 'Patna', 'Jeevak', NULL, NULL, '2021-05-31 20:50:05', '2021-05-31 20:50:05', '2021-05-31 22:20:05', 0, 'Abbott', '0d09919c5c37d469d9c45a067519cf9d'),
(101, 'Dr Amit K Jha', 'amitssudmc@hmail.com', 'Purnea', 'Max7', NULL, NULL, '2021-05-31 20:50:58', '2021-05-31 20:50:58', '2021-05-31 22:20:58', 0, 'Abbott', 'dd6a96575d451ab182b0cc5c55c820fb'),
(102, 'Ayan Manik', 'manik.ayamnanik3@gmail.com', 'Purnea ', 'Max7', NULL, NULL, '2021-05-31 20:51:54', '2021-05-31 20:51:54', '2021-05-31 22:21:54', 0, 'Abbott', '1cbf4ddbd6332b9373fd00f2e1dd19a0'),
(103, 'Dr KASHINATH GHOSH HAZRA', 'drkghazra@gmail.com', 'KOLKATA', 'RN TAGORE INTERNATIONAL INSTITUTE OF  CARDIAC SCIENCE ', NULL, NULL, '2021-05-31 20:53:47', '2021-05-31 20:53:47', '2021-05-31 21:20:36', 0, 'Abbott', 'cba14cba3147e24ee767a8eabcc46633'),
(104, 'Snehil Goswami', 'snehil.1508@gmail.com', 'Raipur', 'NH MMI RAIPUR', NULL, NULL, '2021-05-31 20:54:11', '2021-05-31 20:54:11', '2021-05-31 22:24:11', 0, 'Abbott', '37004c7cbce9c3afa42696d88b4ce26c'),
(105, 'SACHIT ROY', 'sachit.roy@abbott.com', 'Patna', 'AV', NULL, NULL, '2021-05-31 20:56:49', '2021-05-31 20:56:49', '2021-05-31 22:26:49', 0, 'Abbott', '59bf6251267ffd4ad8b141c2cc32eeb9'),
(106, 'Sunil Ch Roy', 'sunil.roy@abbott.com', 'BHUBANESWAR', 'AV', NULL, NULL, '2021-05-31 21:04:23', '2021-05-31 21:04:23', '2021-05-31 21:54:30', 0, 'Abbott', 'c20d6790b3894d1be01044c4bafa8adf'),
(107, 'MANAS RANJAN BARIK', 'manasbarik86@gmail.com', 'BHUBANESWAR', 'SUM ULTIMATE MEDICARE', NULL, NULL, '2021-05-31 21:12:10', '2021-05-31 21:12:10', '2021-05-31 21:23:11', 0, 'Abbott', '286e8d216de94c021b524f8c47889a66'),
(108, 'Dr Somnath', 'somudoccsskm@gmail.com', 'Patna', 'Max7', NULL, NULL, '2021-05-31 21:13:41', '2021-05-31 21:13:41', '2021-05-31 22:43:41', 0, 'Abbott', 'd3b7203b7a8208e9100737d6beadbca8'),
(109, 'Ravi Pd', 'raviprasadraj@gmail.com', 'Patna', 'Paras', NULL, NULL, '2021-05-31 21:15:50', '2021-05-31 21:15:50', '2021-05-31 22:45:50', 0, 'Abbott', '7965c7f97e842c2ddf514472b42abef0'),
(110, 'Dibyajyoti Mishra', 'dibyajyoti.mishra@kims.ac.in', 'Bhubaneswar ', 'Kims ', NULL, NULL, '2021-05-31 21:18:52', '2021-05-31 21:18:52', '2021-05-31 22:48:52', 0, 'Abbott', '32e293138a9783210c7d035804fd84bb'),
(111, 'Dr Shravan Kumar', 'sskumarshravan6591@gmail.com', 'Patna', 'Paras', NULL, NULL, '2021-05-31 21:25:44', '2021-05-31 21:25:44', '2021-05-31 22:55:44', 0, 'Abbott', '456e0637022b24846c3a41ec5dc74ae2'),
(112, 'Dr Shaheen', 'drshaheenahd445@gmail.com', 'Patna', 'AIIMS', NULL, NULL, '2021-05-31 21:29:15', '2021-05-31 21:29:15', '2021-05-31 22:59:15', 0, 'Abbott', '746d6d77ef5cfc1611524dfd5c6f206a'),
(113, 'Kamlesh Kumar', 'kamleshyadavtechno@gmail.com', 'Patna', 'AIIMS', NULL, NULL, '2021-05-31 21:30:33', '2021-05-31 21:30:33', '2021-05-31 23:00:33', 0, 'Abbott', 'eb5d7378a1732bbfb9c6eb6f1167ffcc'),
(114, 'Satyaranjan  Mohanty', 'satya.bapun@gmail.com', 'BHUBANESWAR', 'Apollo Hospitals ', NULL, NULL, '2021-05-31 21:33:25', '2021-05-31 21:33:25', '2021-05-31 23:03:25', 0, 'Abbott', '0006c2464a3a4a595206180a9a1b3df1'),
(115, 'Rahul kolhekar ', 'rahul.vkolhekar@abbott.com', 'Kolkata ', 'Av', NULL, NULL, '2021-05-31 21:57:07', '2021-05-31 21:57:07', '2021-05-31 21:57:22', 0, 'Abbott', '403eea942e033acf95a5c5ffc0a6718a'),
(116, 'Uttam Kumar Ojha', 'uttamojha.ctc@gmail.com', 'Bhubaneswar', 'Apollo hospitals', NULL, NULL, '2021-06-04 19:27:52', '2021-06-04 19:27:52', '2021-06-04 20:57:52', 1, 'Abbott', '99bd7f028c2838a2f8d6630c575ff961'),
(117, 'S Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-06-06 11:18:35', '2021-06-06 11:18:35', '2021-06-06 12:48:35', 1, 'Abbott', 'c1d01cbb66a4a864853d63ac5c321a9d');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
